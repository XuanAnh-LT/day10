<?php 
$allquestion =    
            array(
            '1' => array(
                    array(
                            'text' => 'Câu hỏi 1: Hãy chọn A_1', 
                            'result' => -1
                    ),
                    array(
                            'text' => 'Đáp án A_1', 
                            'result' => 1
                    ),
                                        
                    array(
                            'text' => 'Đáp án B_1', 
                            'result' => 0
                    ),
                    array(
                            'text' => 'Đáp án C_1', 
                            'result' => 0
                    ),
                    array(
                            'text' => 'Đáp án D_1', 
                            'result' => 0
                    ),
                ),

            '2'=>   array(
                        array(
                            'text' => 'Câu hỏi 2: Hãy chọn B_2', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_2', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_2', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án C_2', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_2', 
                            'result' => 0
                        ),
                    ),  

            '3' =>  array(
                        array(
                            'text' => 'Câu hỏi 3: Hãy chọn C_3', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_3', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_3', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_3', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_3', 
                            'result' => 0
                        ),
                    ),  

            '4' =>  array(
                        array(
                            'text' => 'Câu hỏi 4: Hãy chọn D_4', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_4', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_4', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_4', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_4', 
                            'result' => 1
                        ),
                    ),  

            '5' =>  array(
                        array(
                            'text' => 'Câu hỏi 5: Hãy chọn C_5', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_5', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_5', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_5', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_5', 
                            'result' => 0
                        ),
                    ),
                    
            '6' =>  array(
                        array(
                            'text' => 'Câu hỏi 6: Hãy chọn A_6', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_6', 
                            'result' => 1
                        ),
                                        
                        array(
                            'text' => 'Đáp án B_6', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_6', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_6', 
                            'result' => 0
                        ),
                    ),  

            '7' =>  array(
                        array(
                            'text' => 'Câu hỏi 7: Hãy chọn B_7', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_7', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_7', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án C_7', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_7', 
                            'result' => 0
                        ),
                    ),  

            '8' =>  array(
                        array(
                            'text' => 'Câu hỏi 8: Hãy chọn C_8', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_8', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_8', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_8', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_8', 
                            'result' => 0
                        ),
                    ),  

            '9' =>  array(
                        array(
                            'text' => 'Câu hỏi 9: Hãy chọn D_9', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_9', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_9', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_9', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án D_9', 
                            'result' => 1
                        ),
                    ),  

            '10' => array(
                        array(
                            'text' => 'Câu hỏi 10: Hãy chọn C_10', 
                            'result' => -1
                        ),
                        array(
                            'text' => 'Đáp án A_10', 
                            'result' => 0
                        ),
                        
                        array(
                            'text' => 'Đáp án B_10', 
                            'result' => 0
                        ),
                        array(
                            'text' => 'Đáp án C_10', 
                            'result' => 1
                        ),
                        array(
                            'text' => 'Đáp án D_10', 
                            'result' => 0
                        ),
                    )
                )
?>
